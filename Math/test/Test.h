
#pragma once

#include <QtTest>




class TestGrimMath : public QObject
{
	Q_OBJECT

private slots:
	void testAddInt32();
	void testAddInt32Overflow();

	void testSubInt32();
	void testSubInt32Overflow();

	void testIncInt32();
	void testIntInt32Overflow();

	void testDecInt32();
	void testDecInt32Overflow();

	void testMulInt32();
	void testMulInt32Overflow();

	void testDivInt32();
	void testDivInt32Overflow();

	void testMulInt64();
	void testMulInt64Overflow();

	void testMulReal32();

	void testRoundReal32();
	void testReal32Literals();
};
