
#pragma once

#include "Global.h"
#include "Real.h"




namespace Grim {
namespace Math {




template <typename T>
T sqrt( const T & value );

template <typename T>
GRIM_MATH_EXPORT T distance( T a, T b );

GRIM_MATH_EXPORT Real32 angleBetween( Real32 a, Real32 b );




} // namespace Math
} // namespace Grim
