
#pragma once

#include "Global.h"

#include <Grim/Debug/Log.h>




namespace Grim { namespace Math {



template <typename T> class _IntInfo {};
template <typename T> class _IntTypeForInteger {};
template <int Size> class _IntTypeForSize {};
template <typename T> class _Int;

template <typename A, typename B>
class _IntTypeForMax
{
public:
	constexpr inline static int max()
	{
		return qMax(
				sizeof(typename _Int<typename _IntTypeForInteger<A>::Type>::Type),
				sizeof(typename _Int<typename _IntTypeForInteger<B>::Type>::Type));
	}

	typedef typename _IntTypeForSize<max()>::Type IntType;
	typedef _Int<IntType> Type;
};




#define GRIM_MATH_INT_DEFINE_UNARY_OPERATOR(OP) \
	template <typename K> inline _Int<T> & operator OP( const _Int<K> & other ) \
	{ \
		static_assert(sizeof(Type) > sizeof(typename _Int<K>::Type), "Right Int size must be less than target Int size"); \
		return *this OP _Int<T>::fromValue( other.toValue() ); \
	} \
	\
	template <typename K> inline _Int<T> & operator OP( const K & other ) \
	{ \
		typedef _Int<typename _IntTypeForInteger<K>::Type> I; \
		static_assert(sizeof(Type) >= sizeof(typename I::Type), "Right Int size must be less or equal than target Int size"); \
		return *this OP _Int<T>::fromValue( other ); \
	}

#define GRIM_MATH_INT_DECLARE_COMPARISON_OPERATOR(OP) \
	template <typename K, typename L> friend inline bool operator OP( const _Int<K> & a, const _Int<L> & b ); \
	template <typename K, typename L> friend inline bool operator OP( const _Int<K> & a, const L & b ); \
	template <typename K, typename L> friend inline bool operator OP( const L & a, const _Int<K> & b );

#define GRIM_MATH_INT_DEFINE_COMPARISON_OPERATOR(OP) \
	template <typename K, typename L> inline bool operator OP( const _Int<K> & a, const _Int<L> & b ) \
	{ \
		typedef typename _IntTypeForMax<typename _Int<K>::Type, typename _Int<L>::Type>::Type I; \
		return I::fromValue( a.toValue() ) OP I::fromValue( b.toValue() ); \
	} \
	\
	template <typename K, typename L> inline bool operator OP( const _Int<K> & a, const L & b ) \
	{ \
		typedef typename _IntTypeForMax<typename _Int<K>::Type, L>::Type I; \
		return I::fromValue( a.toValue() ) OP I::fromValue( b ); \
	} \
	\
	template <typename K, typename L> inline bool operator OP( const L & a, const _Int<K> & b ) \
	{ return b OP a; }

#define GRIM_MATH_INT_DECLARE_BINARY_OPERATOR(OP) \
	template <typename K, typename L> friend inline typename _IntTypeForMax<typename _Int<K>::Type, typename _Int<L>::Type>::Type operator OP( const _Int<K> & a, const _Int<L> & b ); \
	template <typename K, typename L> friend inline typename _IntTypeForMax<typename _Int<K>::Type, typename _Int<typename _IntTypeForInteger<L>::Type>::Type>::Type operator OP( const _Int<K> & a, const L & b ); \
	template <typename K, typename L> friend inline typename _IntTypeForMax<typename _Int<K>::Type, typename _Int<typename _IntTypeForInteger<L>::Type>::Type>::Type operator OP( const L & a, const _Int<K> & b );

#define GRIM_MATH_INT_DEFINE_BINARY_OPERATOR(OP) \
	template <typename K, typename L> inline typename _IntTypeForMax<typename _Int<K>::Type, typename _Int<L>::Type>::Type operator OP( const _Int<K> & a, const _Int<L> & b ) \
	{ \
		typedef _IntTypeForMax<typename _Int<K>::Type, L> I; \
		return I::fromValue( a.toValue() ) OP I::fromValue( b.toValue() ); \
	} \
	\
	template <typename K, typename L> inline typename _IntTypeForMax<typename _Int<K>::Type, typename _Int<typename _IntTypeForInteger<L>::Type>::Type>::Type operator OP( const _Int<K> & a, const L & b ) \
	{ \
		typedef typename _IntTypeForMax<typename _Int<K>::Type, L>::Type I; \
		return I::fromValue( a.toValue() ) OP I::fromValue( b ); \
	} \
	\
	template <typename K, typename L> inline typename _IntTypeForMax<typename _Int<K>::Type, typename _Int<typename _IntTypeForInteger<L>::Type>::Type>::Type operator OP( const L & a, const _Int<K> & b ) \
	{ return b OP a; }



template <typename T>
class _Int
{
public:
	typedef typename _IntInfo<T>::Type Type;

	static constexpr _Int<T> min();
	static constexpr _Int<T> max();
	static constexpr _Int<T> one();

	constexpr _Int();
	constexpr _Int( const _Int<T> & value );
	template <typename K> _Int( const _Int<K> & value );

	_Int<T> & operator=( const _Int<T> & value );
	template <typename K> _Int<T> & operator=( const _Int<K> & value );

	_Int<T> & operator+=( const _Int<T> & other );
	_Int<T> & operator-=( const _Int<T> & other );
	_Int<T> & operator*=( const _Int<T> & other );
	_Int<T> & operator/=( const _Int<T> & other );
	_Int<T> & operator%=( const _Int<T> & other );

	void operator++();
	void operator++( int );
	void operator--();
	void operator--( int );

	static constexpr _Int<T> fromValue( const Type & value );
	constexpr Type toValue() const;

	template <typename K> friend inline bool operator==( const _Int<K> & a, const _Int<K> & b );
	template <typename K> friend inline bool operator!=( const _Int<K> & a, const _Int<K> & b );
	template <typename K> friend inline bool operator<( const _Int<K> & a, const _Int<K> & b );
	template <typename K> friend inline bool operator>( const _Int<K> & a, const _Int<K> & b );
	template <typename K> friend inline bool operator<=( const _Int<K> & a, const _Int<K> & b );
	template <typename K> friend inline bool operator>=( const _Int<K> & a, const _Int<K> & b );

	template <typename K> friend inline _Int<K> operator-( const _Int<K> & a );

	template <typename K> friend inline _Int<K> operator+( const _Int<K> & a, const _Int<K> & b );
	template <typename K> friend inline _Int<K> operator-( const _Int<K> & a, const _Int<K> & b );
	template <typename K> friend inline _Int<K> operator*( const _Int<K> & a, const _Int<K> & b );
	template <typename K> friend inline _Int<K> operator/( const _Int<K> & a, const _Int<K> & b );
	template <typename K> friend inline _Int<K> operator%( const _Int<K> & a, const _Int<K> & b );

	GRIM_MATH_INT_DEFINE_UNARY_OPERATOR(+=)
	GRIM_MATH_INT_DEFINE_UNARY_OPERATOR(-=)
	GRIM_MATH_INT_DEFINE_UNARY_OPERATOR(*=)
	GRIM_MATH_INT_DEFINE_UNARY_OPERATOR(/=)
	GRIM_MATH_INT_DEFINE_UNARY_OPERATOR(%=)

	GRIM_MATH_INT_DECLARE_COMPARISON_OPERATOR(==)
	GRIM_MATH_INT_DECLARE_COMPARISON_OPERATOR(!=)
	GRIM_MATH_INT_DECLARE_COMPARISON_OPERATOR(>)
	GRIM_MATH_INT_DECLARE_COMPARISON_OPERATOR(<)
	GRIM_MATH_INT_DECLARE_COMPARISON_OPERATOR(>=)
	GRIM_MATH_INT_DECLARE_COMPARISON_OPERATOR(<=)

	GRIM_MATH_INT_DECLARE_BINARY_OPERATOR(+)
	GRIM_MATH_INT_DECLARE_BINARY_OPERATOR(-)
	GRIM_MATH_INT_DECLARE_BINARY_OPERATOR(*)
	GRIM_MATH_INT_DECLARE_BINARY_OPERATOR(/)
	GRIM_MATH_INT_DECLARE_BINARY_OPERATOR(%)

private:
	explicit constexpr _Int( const Type & value );

private:
	Type value_;
};




class _IntType32;
class _IntType64;

template <>
class _IntInfo<_IntType32>
{
public:
	typedef qint32 Type;
	typedef quint32 UnsignedType;
	typedef _IntType64 HiType;
};

template <>
class _IntTypeForInteger<qint32>
{
public:
	typedef _IntType32 Type;
};

template <>
class _IntTypeForSize<4>
{
public:
	typedef _IntType32 Type;
};

template <>
class _IntInfo<_IntType64>
{
public:
	typedef qint64 Type;
	typedef quint64 UnsignedType;
};

template <>
class _IntTypeForInteger<qint64>
{
public:
	typedef _IntType64 Type;
};

template <>
class _IntTypeForSize<8>
{
public:
	typedef _IntType64 Type;
};

typedef _Int<_IntType32> Int32;
typedef _Int<_IntType64> Int64;




template <typename T>
constexpr _Int<T> _Int<T>::one()
{ return _Int<T>::fromValue( 1 ); }

template <typename T>
constexpr _Int<T>::_Int( const Type & value ) :
	value_( value )
{}

template <typename T>
inline constexpr _Int<T>::_Int() :
	value_( 0 )
{}

template <typename T>
inline constexpr _Int<T>::_Int( const _Int<T> & value ) :
	value_( value.value_ )
{}

template <typename T>
template <typename K>
inline _Int<T>::_Int( const _Int<K> & value ) :
	value_( static_cast<Type>(value.toValue()) )
{
	static_assert(sizeof(Type) > sizeof(typename _Int<K>::Type),
			"Argument Int size must be less than target Int size");
}

template <typename T>
inline _Int<T> & _Int<T>::operator=( const _Int<T> & value )
{ value_ = value.value_; return *this; }

template <typename T>
template <typename K>
inline _Int<T> & _Int<T>::operator=( const _Int<K> & value )
{
	static_assert(sizeof(Type) > sizeof(typename _Int<K>::Type),
			"Argument Int size must be less than target Int size");
	value_ = static_cast<Type>(value.value_);
	return *this;
}

template <typename T>
inline _Int<T> & _Int<T>::operator+=( const _Int<T> & other )
{ value_ = add_int( value_, other.value_ ); return *this; }

template <typename T>
inline _Int<T> & _Int<T>::operator-=( const _Int<T> & other )
{ value_ = sub_int( value_, other.value_ ); return *this; }

template <typename T>
inline _Int<T> & _Int<T>::operator*=( const _Int<T> & other )
{ value_ = mul_int( value_, other.value_ ); return *this; }

template <typename T>
inline _Int<T> & _Int<T>::operator/=( const _Int<T> & other )
{ value_ = div_int( value_, other.value_ ); return *this; }

template <typename T>
inline _Int<T> & _Int<T>::operator%=( const _Int<T> & other )
{ value_ = mod_int( value_, other.value_ ); return *this; }

template <typename T>
void _Int<T>::operator++()
{ inc_int( value_ ); }

template <typename T>
void _Int<T>::operator++( int )
{ inc_int( value_ ); }

template <typename T>
void _Int<T>::operator--()
{ dec_int( value_ ); }

template <typename T>
void _Int<T>::operator--( int )
{ dec_int( value_ ); }

template <typename T>
constexpr _Int<T> _Int<T>::fromValue( const Type & value )
{ return _Int<T>( value ); }

template <typename T>
inline constexpr typename _Int<T>::Type _Int<T>::toValue() const
{ return value_; }

template <typename T>
inline bool operator==( const _Int<T> & a, const _Int<T> & b )
{ return a.value_ == b.value_; }

template <typename T>
inline bool operator!=( const _Int<T> & a, const _Int<T> & b )
{ return a.value_ != b.value_; }

template <typename T>
inline bool operator<( const _Int<T> & a, const _Int<T> & b )
{ return a.value_ < b.value_; }

template <typename T>
inline bool operator>( const _Int<T> & a, const _Int<T> & b )
{ return a.value_ > b.value_; }

template <typename T>
inline bool operator<=( const _Int<T> & a, const _Int<T> & b )
{ return a.value_ <= b.value_; }

template <typename T>
inline bool operator>=( const _Int<T> & a, const _Int<T> & b )
{ return a.value_ >= b.value_; }

template <typename T>
inline _Int<T> operator-( const _Int<T> & a )
{ return _Int<T>::fromValue( minus_int( a.value_ ) ); }

template <typename T>
inline _Int<T> operator+( const _Int<T> & a, const _Int<T> & b )
{ return _Int<T>::fromValue( add_int( a.value_, b.value_ ) ); }

template <typename T>
inline _Int<T> operator-( const _Int<T> & a, const _Int<T> & b )
{ return _Int<T>::fromValue( sub_int( a.value_, b.value_ ) ); }

template <typename T>
inline _Int<T> operator*( const _Int<T> & a, const _Int<T> & b )
{ return _Int<T>::fromValue( mul_int( a.value_, b.value_ ) ); }

template <typename T>
inline _Int<T> operator/( const _Int<T> & a, const _Int<T> & b )
{ return _Int<T>::fromValue( div_int( a.value_, b.value_ ) ); }

template <typename T>
inline _Int<T> operator%( const _Int<T> & a, const _Int<T> & b )
{ return _Int<T>::fromValue( mod_int( a.value_, b.value_ ) ); }

GRIM_MATH_INT_DEFINE_COMPARISON_OPERATOR(==)
GRIM_MATH_INT_DEFINE_COMPARISON_OPERATOR(!=)
GRIM_MATH_INT_DEFINE_COMPARISON_OPERATOR(>)
GRIM_MATH_INT_DEFINE_COMPARISON_OPERATOR(<)
GRIM_MATH_INT_DEFINE_COMPARISON_OPERATOR(>=)
GRIM_MATH_INT_DEFINE_COMPARISON_OPERATOR(<=)

GRIM_MATH_INT_DEFINE_BINARY_OPERATOR(+)
GRIM_MATH_INT_DEFINE_BINARY_OPERATOR(-)
GRIM_MATH_INT_DEFINE_BINARY_OPERATOR(*)
GRIM_MATH_INT_DEFINE_BINARY_OPERATOR(/)
GRIM_MATH_INT_DEFINE_BINARY_OPERATOR(%)




#undef GRIM_MATH_INT_DEFINE_UNARY_OPERATOR
#undef GRIM_MATH_INT_DECLARE_COMPARISON_OPERATOR
#undef GRIM_MATH_INT_DEFINE_COMPARISON_OPERATOR
#undef GRIM_MATH_INT_DECLARE_BINARY_OPERATOR
#undef GRIM_MATH_INT_DEFINE_BINARY_OPERATOR




// 32 bit overloads
template <>
constexpr Int32 _Int<_IntType32>::min()
{ return Int32::fromValue( kInt32Min ); }

template <>
constexpr Int32 _Int<_IntType32>::max()
{ return Int32::fromValue( kInt32Max ); }


// 64 bit overloads
template <>
constexpr Int64 _Int<_IntType64>::min()
{ return Int64::fromValue( kInt64Min ); }

template <>
constexpr Int64 _Int<_IntType64>::max()
{ return Int64::fromValue( kInt64Max ); }




}} // Grim::Math




#if 0
template <>
inline Grim::Math::Int32 qAbs<Grim::Math::Int32>( const Grim::Math::Int32 & t )
{ return t >= Grim::Math::Int32() ? t : -t; }

template <>
inline Grim::Math::Int64 qAbs<Grim::Math::Int64>( const Grim::Math::Int64 & t )
{ return t >= Grim::Math::Int64() ? t : -t; }
#endif




namespace Grim { namespace Debug {

template <typename T>
inline void operator<<( Log::Append & append, const Grim::Math::_Int<T> & i )
{
	append.log().append( i.toValue() );
}

}} // Grim::Debug
