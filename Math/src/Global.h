
#pragma once

#include <qglobal.h>




#ifdef Q_OS_WIN
#	ifdef GRIM_MATH_STATIC
#		define GRIM_MATH_EXPORT
#	else
#		ifdef GRIM_MATH_BUILD
#			define GRIM_MATH_EXPORT Q_DECL_EXPORT
#		else
#			define GRIM_MATH_EXPORT Q_DECL_IMPORT
#		endif
#	endif
#else
#	define GRIM_MATH_EXPORT __attribute__ ((visibility("default")))
#endif




#ifdef GRIM_MATH_USE_EXCEPTIONS
#	define GRIM_MATH_CHECK( cond ) \
	{ if ( Q_UNLIKELY(!(cond)) ) throw CheckException( __LINE__, __FILE__, Q_FUNC_INFO ); }
#else
#	define GRIM_MATH_CHECK( cond ) \
		Q_ASSERT_X( (cond), "Grim::Math", "Overflow in math arithmetic" )
#endif




namespace Grim { namespace Math {




static const qint32 kInt32Min   = -2147483647 - 1;
static const qint32 kInt32Max   = 2147483647;
static const quint32 kUint32Max = 4294967295U;

static const qint64 kInt64Min = -9223372036854775807L - 1L;
static const qint64 kInt64Max = 9223372036854775807L;




template <typename T> int sign( T a );
template <typename T> bool hasSign( T a );

template <typename T> GRIM_MATH_EXPORT T add_int_safe( T a, T b );
template <typename T> GRIM_MATH_EXPORT T sub_int_safe( T a, T b );
template <typename T> GRIM_MATH_EXPORT void inc_int_safe( T & a );
template <typename T> GRIM_MATH_EXPORT void dec_int_safe( T & a );
template <typename T> GRIM_MATH_EXPORT T minus_int_safe( T a );
template <typename T> GRIM_MATH_EXPORT T mul_int_safe( T a, T b );
template <typename T> GRIM_MATH_EXPORT T div_int_safe( T a, T b );
template <typename T> GRIM_MATH_EXPORT T mod_int_safe( T a, T b );

GRIM_MATH_EXPORT qint32 mul_real_safe_32( qint32 a, qint32 b );
GRIM_MATH_EXPORT qint64 mul_real_safe_64( qint64 a, qint64 b );

GRIM_MATH_EXPORT qint32 div_real_safe_32( qint32 a, qint32 b );
GRIM_MATH_EXPORT qint64 div_real_safe_64( qint64 a, qint64 b );

template <typename T> T add_int( T a, T b );
template <typename T> T sub_int( T a, T b );
template <typename T> void inc_int( T & a );
template <typename T> void dec_int( T & a );
template <typename T> T minus_int( T a );
template <typename T> T mul_int( T a, T b );
template <typename T> T div_int( T a, T b );
template <typename T> T mod_int( T a, T b );

template <typename T> T mul_real( T a, T b );
template <typename T> T div_real( T a, T b );




class GRIM_MATH_EXPORT CheckException
{
public:
	CheckException( int line, const char * file, const char * function );

private:
	const int line_;
	const char * const file_;
	const char * const function_;
};




template <typename T>
inline int sign( T a )
{ return a < 0 ? -1 : 1; }

template <typename T>
inline bool hasSign( T a )
{ return (a & (1 << (sizeof(a)*8 - 1))) != 0; }


template <typename T>
inline T add_int( const T a, const T b )
{
#ifdef GRIM_MATH_USE_SAFE_ARITHMETIC
	return add_int_safe( a, b );
#else
	return a + b;
#endif
}


template <typename T>
inline T sub_int( const T a, const T b )
{
#ifdef GRIM_MATH_USE_SAFE_ARITHMETIC
	return sub_int_safe( a, b );
#else
	return a - b;
#endif
}


template <typename T>
inline void inc_int( T & a )
{
#ifdef GRIM_MATH_USE_SAFE_ARITHMETIC
	inc_int_safe( a );
#else
	a++;
#endif
}


template <typename T>
inline void dec_int( T & a )
{
#ifdef GRIM_MATH_USE_SAFE_ARITHMETIC
	dec_int_safe( a );
#else
	a--;
#endif
}


template <typename T>
inline T minus_int( const T a )
{
#ifdef GRIM_MATH_USE_SAFE_ARITHMETIC
	return minus_int_safe( a );
#else
	return -a;
#endif
}


template <typename T>
inline T mul_int( const T a, const T b )
{
#ifdef GRIM_MATH_USE_SAFE_ARITHMETIC
	return mul_int_safe( a, b );
#else
	return a * b;
#endif
}


template <typename T>
inline T div_int( const T a, const T b )
{
#ifdef GRIM_MATH_USE_SAFE_ARITHMETIC
	return div_int_safe( a, b );
#else
	return a / b;
#endif
}


template <typename T>
inline T mod_int( const T a, const T b )
{
#ifdef GRIM_MATH_USE_SAFE_ARITHMETIC
	return mod_int_safe( a, b );
#else
	return a % b;
#endif
}


template <>
inline qint32 mul_real<qint32>( const qint32 a, const qint32 b )
{
#ifdef GRIM_MATH_USE_SAFE_ARITHMETIC
	return mul_real_safe_32( a, b );
#else
	return static_cast<qint32>( (static_cast<qint64>(a)*b) >> 32 );
#endif
}


template <>
inline qint64 mul_real<qint64>( const qint64 a, const qint64 b )
{
#ifdef GRIM_MATH_USE_SAFE_ARITHMETIC
	return mul_real_safe_64( a, b );
#else
	// FIXME: Implement this.
	return 0;
#endif
}


template <typename T>
inline T div_real( const T a, const T b )
{
	return 0;
}




}} // Grim::Math
