
#include "Global.h"

#include "Debug.h"




namespace Grim { namespace Math {




CheckException::CheckException( const int line, const char * const file, const char * const function ) :
	line_( line ),
	file_( file ),
	function_( function )
{
	GRIM_WARNING << file << function << line;
}




template <>
qint32 add_int_safe<qint32>( const qint32 a, const qint32 b )
{
	GRIM_MATH_CHECK( ((a^b) | (((a^(~(a^b) & kInt32Min)) + b)^b)) < 0 );

	return a + b;
}


template <>
qint32 sub_int_safe<qint32>( const qint32 a, const qint32 b )
{
	GRIM_MATH_CHECK( ((a^b) & (((a ^ ((a^b) & (qint32(1) << (sizeof(qint32)*8-1))))-b)^b)) >= 0 );

	return a - b;
}


template <>
void inc_int_safe<qint32>( qint32 & a )
{
	GRIM_MATH_CHECK( a < kInt32Max );

	a++;
}


template <>
void dec_int_safe<qint32>( qint32 & a )
{
	GRIM_MATH_CHECK( a > kInt32Min );

	a--;
}


template <>
qint32 minus_int_safe<qint32>( const qint32 a )
{
	GRIM_MATH_CHECK( a != kInt32Min );

	return -a;
}


template <>
qint32 mul_int_safe<qint32>( const qint32 a, const qint32 b )
{
	const qint64 result = qint64(a) * qint64(b);
	GRIM_MATH_CHECK( result <= qint64(kInt32Max) && result >= qint64(kInt32Min) );

	return static_cast<qint32>( result );
}


template <>
qint32 div_int_safe<qint32>( const qint32 a, const qint32 b )
{
	GRIM_MATH_CHECK( b != 0 && (a != kInt32Min || b != -1) );

	return a / b;
}


template <>
qint32 mod_int_safe<qint32>( const qint32 a, const qint32 b )
{
	GRIM_MATH_CHECK( b != 0 && (a != kInt32Min || b != -1) );

	return a % b;
}




template <>
qint64 add_int_safe<qint64>( const qint64 a, const qint64 b )
{
	GRIM_MATH_CHECK( ((a^b) | (((a^(~(a^b) & kInt64Min)) + b)^b)) < 0 );

	return a + b;
}


template <>
qint64 sub_int_safe<qint64>( const qint64 a, const qint64 b )
{
	GRIM_MATH_CHECK( ((a^b) & (((a ^ ((a^b) & (qint64(1) << (sizeof(qint64)*CHAR_BIT-1))))-b)^b)) >= 0 );

	return a - b;
}


template <>
void inc_int_safe<qint64>( qint64 & a )
{
	GRIM_MATH_CHECK( a < kInt64Max );

	a++;
}


template <>
void dec_int_safe<qint64>( qint64 & a )
{
	GRIM_MATH_CHECK( a > kInt64Min );

	a--;
}


template <>
qint64 minus_int_safe<qint64>( const qint64 a )
{
	GRIM_MATH_CHECK( a != kInt64Min );

	return -a;
}


template <>
qint64 mul_int_safe<qint64>( const qint64 a, const qint64 b )
{
	if ( a > 0 )
	{
		if ( b > 0 )
		{
			GRIM_MATH_CHECK( a <= kInt64Max/b );
		}
		else
		{
			GRIM_MATH_CHECK( b >= kInt64Min/a );
		}
	}
	else
	{
		if ( b > 0 )
		{
			GRIM_MATH_CHECK( a >= kInt64Min/b );
		}
		else
		{
			GRIM_MATH_CHECK( (a == 0) || (b >= kInt64Max/a) );
		}
	}

	return a*b;
}


template <>
qint64 div_int_safe<qint64>( const qint64 a, const qint64 b )
{
	GRIM_MATH_CHECK( b != 0 && (a != kInt64Min || b != -1) );

	return a / b;
}


template <>
qint64 mod_int_safe<qint64>( const qint64 a, const qint64 b )
{
	GRIM_MATH_CHECK( b != 0 && (a != kInt64Min || b != -1) );

	return a % b;
}


qint32 mul_real_safe_32( const qint32 a, const qint32 b )
{
	qint64 product = static_cast<qint64>(a) * b;

	// The upper 17 bits should all be the same (the sign).
	quint32 upper = (product >> 47);

	if ( product < 0 )
	{
		GRIM_MATH_CHECK( !~upper );

#ifdef GRIM_MATH_USE_REAL_ROUNDING
		// This adjustment is required in order to round -1/2 correctly
		product--;
#endif
	}
	else
	{
		GRIM_MATH_CHECK( !upper );
	}

#ifndef GRIM_MATH_USE_REAL_ROUNDING
	return product >> 16;
#else
	qint32 result = product >> 16;
	result += (product & 0x8000) >> 15;

	return result;
#endif
}


qint64 mul_real_safe_64( const qint64 a, const qint64 b )
{
	// Each argument is divided to 32-bit parts.
	//          AB
	//      *   CD
	// -----------
	//          BD  32 * 32 -> 64 bit products
	//         CB
	//         AD
	//        AC
	//       |----| 128 bit product
	qint64  A = (a >> 32);
	qint64  C = (b >> 32);
	quint64 B = (a & 0xFFFFFFFF);
	quint64 D = (b & 0xFFFFFFFF);

	qint64 AC = A*C;
	qint64 AD_CB = A*D + C*B;
	quint64 BD = B*D;

	qint64 product_hi = AC + (AD_CB >> 32);

	// Handle carry from lower 64 bits to upper part of result.
	quint64 ad_cb_temp = AD_CB << 32;
	quint64 product_lo = BD + ad_cb_temp;
	if ( product_lo < BD )
		product_hi++;

	GRIM_MATH_CHECK( product_hi >> 63 == product_hi >> 31 );

#ifndef GRIM_MATH_USE_REAL_ROUNDING
	return (product_hi << 32) | (product_lo >> 32);
#else
	// Subtracting 0x80000000 (= 0.5) and then using signed right shift
	// achieves proper rounding to result-1, except in the corner
	// case of negative numbers and lowest word = 0x80000000.
	// To handle that, we also have to subtract 1 for negative numbers.
	quint64 product_lo_tmp = product_lo;
	product_lo -= 0x80000000;
	product_lo -= static_cast<quint64>(product_hi) >> 63;
	if ( product_lo > product_lo_tmp )
		product_hi--;

	// Discard the lowest 32 bits. Note that this is not exactly the same
	// as dividing by 0x100000000. For example if product = -1, result will
	// also be -1 and not 0. This is compensated by adding +1 to the result
	// and compensating this in turn in the rounding above.
	qint64 result = (product_hi << 32) | (product_lo >> 32);
	result += 1;
	return result;
#endif




#if 0
	const qint64 ah = a >> 32;
	const qint64 bh = b >> 32;

	const quint64 al = a & 0xffffffff;
	const quint64 bl = b & 0xffffffff;

	const qint64 h = ah * bh;
	const qint64 m = ah*bl + bh*al;
	const quint64 l = al * bl;

	qint64 product_hi = h + (m >> 32);

	// Handle carry from lower 64 bits to upper part of result.
	const quint64 m_temp = m << 32;
	quint64 product_lo = l + m_temp;
	if ( product_lo < l )
		product_hi++;

	// The upper 33 bits should all be the same (the sign).
	GRIM_MATH_CHECK( (product_hi >> 63) == (product_hi >> 31) );

#ifndef GRIM_MATH_USE_REAL_ROUNDING
	return (product_hi << 32) | (product_lo >> 32);
#else

	// Subtracting 0x80000000 (= 0.5) and then using signed right shift
	// achieves proper rounding to result-1, except in the corner
	// case of negative numbers and lowest word = 0x80000000.
	// To handle that, we also have to subtract 1 for negative numbers.
	const quint64 product_lo_tmp = product_lo;
	product_lo -= 0x80000000;
	product_lo -= (quint64)product_hi >> 63;
	if ( product_lo > product_lo_tmp )
		product_hi--;

	// Discard the lowest 16 bits. Note that this is not exactly the same
	// as dividing by 0x10000. For example if product = -1, result will
	// also be -1 and not 0. This is compensated by adding +1 to the result
	// and compensating this in turn in the rounding above.
	qint64 result = (product_hi << 32) | (product_lo >> 32);
	result += 1;

	return result;
#endif
#endif
}




}} // Grim::Math
