
#pragma once

#include "Global.h"

#include <Grim/Debug/Log.h>




namespace Grim { namespace Math {




class Bool
{
public:
	Bool();
	Bool( bool value );

	bool value() const;

	operator bool() const;

private:
	bool value_;
};




inline Bool::Bool() :
	value_( false )
{}

inline Bool::Bool( const bool value ) :
	value_( !!value )
{}

inline bool Bool::value() const
{ return value_; }

inline Bool::operator bool() const
{ return value_; }




}} // Grim::Math




namespace Grim { namespace Debug {

template <>
inline void Log::append<Math::Bool>( const Math::Bool & value )
{
	append( value.value() );
}

}} // Grim::Debug
