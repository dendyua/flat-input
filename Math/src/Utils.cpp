
#include "Utils.h"




namespace Grim {
namespace Math {




template <typename T>
_Int<T> sqrt( const _Int<T> & value )
{
	GRIM_MATH_CHECK( value >= 0 );

	typedef typename _IntInfo<T>::UnsignedType U;

	U op  = value.toValue();
	U res = 0;
	// The second-to-top bit is set: use 1u << 14 for uint16_t type; use 1uL<<30 for uint32_t type
	U one = U(1) << (sizeof(U)*8 - 2);

	// "one" starts at the highest power of four <= than the argument.
	while ( one > op )
	{
		one >>= 2;
	}

	while ( one != 0 )
	{
		if ( op >= res + one )
		{
			op -= res + one;
			res += 2*one;
		}
		res >>= 1;
		one >>= 2;
	}

	return _Int<T>::fromValue(res);
}



template <>
Int32 distance<Int32>( const Int32 a, const Int32 b )
{
	Int64 sa = a;
	sa *= a;

	Int64 sb = b;
	sb *= b;

	Int64 s = sa + sb;
	Int64 sr = sqrt( s );

	GRIM_MATH_CHECK( sr.toValue() <= Int32::max().toValue() );

	return Int32::fromValue( static_cast<int>(sr.toValue()) );
}


Real32 angleBetween( Real32 a, Real32 b )
{
	return Real32();
}





template GRIM_MATH_EXPORT Int32 distance<Int32>( Int32, Int32 );





} // namespace Math
} // namespace Grim
