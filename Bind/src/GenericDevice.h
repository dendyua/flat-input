
#pragma once

#include "Bind.h"




namespace Grim {
namespace Bind {




class GenericDevicePrivate;




class GRIM_BIND_EXPORT GenericDevice : public Device
{
	Q_OBJECT

public:
	GenericDevice();
	~GenericDevice();

	// reimplemented from Device
	QString buttonToString( int button ) const;
	QString buttonName( int button ) const;

	QString axisToString( int axis ) const;
	QString axisName( int axis ) const;

private:
	const GenericDevicePrivate * constData() const;
	GenericDevicePrivate * data() const;
};




} // namespace Bind
} // namespace Grim
