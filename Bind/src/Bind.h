
#pragma once

#include "Global.h"

#include <QObject>
#include <QHash>




namespace Grim { namespace Bind {




class Manager;
class DevicePrivate;
class ContextPrivate;
class ManagerPrivate;




class GRIM_BIND_EXPORT Button
{
public:
	Button();

	bool operator==( const Button & button ) const;

	bool isNull() const;

	int deviceId() const;
	int button() const;

	QString toString() const;
	QString name() const;

private:
	Button( int deviceId, int button, const Manager * manager );

private:
	int deviceId_;
	int button_;
	const Manager * manager_;

	friend class Manager;
};




class GRIM_BIND_EXPORT Axis
{
public:
	Axis();

	bool operator==( const Axis & axis ) const;

	bool isNull() const;

	int deviceId() const;
	int axis() const;

	QString toString() const;
	QString name() const;

private:
	Axis( int deviceId, int axis, const Manager * manager );

private:
	int deviceId_;
	int axis_;
	const Manager * manager_;

	friend class Manager;
};




class GRIM_BIND_EXPORT Device : public QObject
{
	Q_OBJECT

public:
	Device();
	Device( DevicePrivate * d );
	~Device();

	virtual QString buttonToString( int button ) const = 0;
	virtual QString buttonName( int button ) const = 0;

	virtual QString axisToString( int axis ) const = 0;
	virtual QString axisName( int axis ) const = 0;

	template <typename T>
	const T * constCastTo() const;

	template <typename T>
	T * castTo() const;

private:
	DevicePrivate * const d_;

	friend class Manager;
};




class GRIM_BIND_EXPORT AxisDirection
{
public:
	AxisDirection();
	AxisDirection( const Axis & axis, bool forward );

	bool operator==( const AxisDirection & other ) const;

	bool isNull() const;

	Axis axis() const;
	bool forward() const;

private:
	Axis axis_;
	bool forward_;
};




class GRIM_BIND_EXPORT Context : public QObject
{
	Q_OBJECT

public:
	~Context();

	void attachButtonToCommand( const Button & button, int command );
	void attachAxisDirectionToCommand( const AxisDirection & axisDirection, int command );
	void attachAxisToCommand( const Axis & axis, int command );

	void detachButton( const Button & button );
	void detachAxis( const Axis & axis );
	void detachAxisDirection( const AxisDirection & axisDirection );
	void detachCommand( int command );
	void detachAll();

	QList<Button> buttonsForCommand( int command ) const;
	QList<AxisDirection> axisDirectionsForCommand( int command ) const;
	QList<Axis> axesForCommand( int command ) const;

signals:
	void bindChanged( int command );

private:
	Context( Manager * manager );

private:
	ContextPrivate * const d_;

	friend class Manager;
	friend class ManagerPrivate;
	friend class ContextPrivate;
};




class GRIM_BIND_EXPORT Manager : public QObject
{
	Q_OBJECT

public:
	Manager( QObject * parent );
	~Manager();

	template<typename T>
	int registerDevice();

	int createDevice( int type );
	void removeDevice( int deviceId );

	Button createButton( int deviceId, int button ) const;
	Axis createAxis( int deviceId, int axis ) const;

	int createCommand();
	int createToggleCommand();
	int createAxisCommand();

	bool isCommandToggle( int command ) const;
	bool isCommandAxis( int command ) const;

#if 0
	int createCommandGroup();
	void addCommandToGroup( int groupId, const QString & command );
	void setCommandGroupEnabled( int groupId, bool enabled );
#endif

	Context * createContext();

	QList<Context*> currentContexts() const;
	void setCurrentContexts( const QList<Context*> & contexts );

	bool handleButton( const Button & button, bool isPressed );
	bool handleAxis( const Axis & axis, qreal value );

signals:
	void commandTriggered( int command );
	void toggleCommandTriggered( int command, bool on );
	void axisCommandTriggered( int command, qreal value );

private:
	int _registerDevice( Device * device );

private:
	ManagerPrivate * const d_;

	friend class Button;
	friend class Axis;
	friend class Context;
	friend class ManagerPrivate;
};




inline Button::Button() :
	deviceId_( 0 ), button_( 0 )
{}

inline Button::Button( const int deviceId, const int button, const Manager * const manager ) :
	deviceId_( deviceId ), button_( button ), manager_( manager )
{}

inline bool Button::operator==( const Button & button ) const
{ return deviceId_ == button.deviceId_ && button_ == button.button_; }

inline bool Button::isNull() const
{ return deviceId_ == 0; }

inline int Button::deviceId() const
{ return deviceId_; }

inline int Button::button() const
{ return button_; }

inline uint qHash( const Button & button )
{ return ::qHash( button.deviceId() ) + ::qHash( button.button() ); }




inline Axis::Axis() :
	deviceId_( 0 ), axis_( 0 )
{}

inline Axis::Axis( const int deviceId, const int axis, const Manager * const manager ) :
	deviceId_( deviceId ), axis_( axis ), manager_( manager )
{}

inline bool Axis::operator==( const Axis & axis ) const
{ return deviceId_ == axis.deviceId_ && axis_ == axis.axis_; }

inline bool Axis::isNull() const
{ return deviceId_ == 0; }

inline int Axis::deviceId() const
{ return deviceId_; }

inline int Axis::axis() const
{ return axis_; }

inline uint qHash( const Axis & axis )
{ return ::qHash( axis.deviceId() ) + ::qHash( axis.axis() ); }




template <typename T>
inline const T * Device::constCastTo() const
{ return static_cast<T*>( d_ ); }

template <typename T>
inline T * Device::castTo() const
{ return static_cast<T*>( d_ ); }




inline AxisDirection::AxisDirection() :
	forward_( false )
{}

inline AxisDirection::AxisDirection( const Axis & axis, const bool forward ) :
	axis_( axis ), forward_( forward )
{}

inline bool AxisDirection::operator==( const AxisDirection & other ) const
{ return axis_ == other.axis_ && forward_ == other.forward_; }

inline bool AxisDirection::isNull() const
{ return axis_.isNull(); }

inline Axis AxisDirection::axis() const
{ return axis_; }

inline bool AxisDirection::forward() const
{ return forward_; }

inline uint qHash( const AxisDirection & axis )
{ return Grim::Bind::qHash( axis.axis() ) + ::qHash( axis.forward() ); }




template<typename T>
inline int Manager::registerDevice()
{ return _registerDevice( new T ); }




}} // Grim::Bind
