
#pragma once

#include "Bind.h"

#include <Grim/Gamepad/Gamepad.h>




namespace Grim { namespace Bind {




class XBoxControllerDevicePrivate;




class GRIM_BIND_EXPORT XBoxControllerDevice : public Device
{
	Q_OBJECT

public:
	enum ButtonType
	{
		Button_Null        = 0,

		Button_A           = 1,
		Button_B           = 2,
		Button_X           = 3,
		Button_Y           = 4,
		Button_LeftBumper  = 5,
		Button_RightBumper = 6,
		Button_Back        = 7,
		Button_Start       = 8,
		Button_LeftStick   = 9,
		Button_RightStick  = 10,
		Button_Mode        = 11,

		Button_TotalButtons
	};

	enum AxisType
	{
		Axis_Null   = 0,

		Axis_LeftX  = 1,
		Axis_LeftY  = 2,
		Axis_RightX = 3,
		Axis_RightY = 4,
		Axis_LeftZ  = 5,
		Axis_RightZ = 6,
		Axis_PovX   = 7,
		Axis_PovY   = 8,

		Axis_TotalAxes
	};

	static ButtonType buttonFromGamepadButton( Gamepad::ButtonType buttonType );
	static AxisType axisFromGamepadAxis( Gamepad::AxisType axisType );

	XBoxControllerDevice();
	~XBoxControllerDevice();

	// reimplemented from Device
	QString buttonToString( int button ) const;
	QString buttonName( int button ) const;

	QString axisToString( int axis ) const;
	QString axisName( int axis ) const;

private:
	const XBoxControllerDevicePrivate * constData() const;
	XBoxControllerDevicePrivate * data() const;
};




}} // Grim::Bind
