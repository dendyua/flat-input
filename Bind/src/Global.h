
#pragma once

#include <qglobal.h>




#ifdef GRIM_BIND_STATIC
#	define GRIM_BIND_EXPORT
#else
#	ifdef GRIM_BIND_BUILD
#		define GRIM_BIND_EXPORT Q_DECL_EXPORT
#	else
#		define GRIM_BIND_EXPORT Q_DECL_IMPORT
#	endif
#endif
