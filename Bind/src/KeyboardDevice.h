
#pragma once

#include "Bind.h"




namespace Grim {
namespace Bind {




class KeyboardDevicePrivate;




class GRIM_BIND_EXPORT KeyboardDevice : public Device
{
	Q_OBJECT

public:
	KeyboardDevice();
	~KeyboardDevice();

	// reimplemented from Device
	QString buttonToString( int button ) const;
	QString buttonName( int button ) const;

	QString axisToString( int axis ) const;
	QString axisName( int axis ) const;

private:
	const KeyboardDevicePrivate * constData() const;
	KeyboardDevicePrivate * data() const;
};




} // namespace Bind
} // namespace Grim
