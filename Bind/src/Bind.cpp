
#include "Bind.h"
#include "Bind_p.h"

#include <QSet>

#include <Grim/Tools/Utils.h>




namespace Grim { namespace Bind {




QString Button::toString() const
{
	return manager_->d_->deviceInfoForId.at( deviceId_ ).device->buttonToString( button_ );
}


QString Button::name() const
{
	return manager_->d_->deviceInfoForId.at( deviceId_ ).device->buttonName( button_ );
}




QString Axis::toString() const
{
	return manager_->d_->deviceInfoForId.at( deviceId_ ).device->axisToString( axis_ );
}


QString Axis::name() const
{
	return manager_->d_->deviceInfoForId.at( deviceId_ ).device->axisName( axis_ );
}




Device::Device() :
	d_( new DevicePrivate )
{
}


Device::Device( DevicePrivate * const d ) :
	d_( d )
{
}


Device::~Device()
{
	delete d_;
}




int ContextPrivate::detachButton( const Button & button )
{
	const int command = commandForButton.take( button );

	if ( command != 0 )
	{
		CommandInfo & commandInfo = commandInfos[ command ];
		Q_ASSERT( commandInfo.boundButtons.count( button ) == 1 );
		commandInfo.boundButtons.removeOne( button );
	}

	return command;
}


int ContextPrivate::detachAxis( const Axis & axis )
{
	const int command = commandForAxis.take( axis );

	if ( command != 0 )
	{
		CommandInfo & commandInfo = commandInfos[ command ];
		Q_ASSERT( commandInfo.boundAxes.count() == 1 );
		commandInfo.boundAxes.removeOne( axis );
	}

	return command;
}


int ContextPrivate::detachAxisDirection( const AxisDirection & axisDirection )
{
	const int command = commandForAxisDirection.take( axisDirection );

	if ( command != 0 )
	{
		CommandInfo & commandInfo = commandInfos[ command ];
		Q_ASSERT( commandInfo.boundAxisDirections.count( axisDirection ) == 1 );
		commandInfo.boundAxisDirections.removeOne( axisDirection );
	}

	return command;
}




Context::Context( Manager * const manager ) :
	d_( new ContextPrivate( this, manager ) )
{
}


Context::~Context()
{
	d_->manager->d_->contexts.removeOne( this );
	d_->manager->d_->currentContexts.removeOne( this );
}


void Context::attachButtonToCommand( const Button & button, const int command )
{
	if ( d_->commandForButton.value( button ) == command )
		return;

	{
		const int previousCommand = d_->detachButton( button );
		if ( previousCommand != 0 )
			emit bindChanged( previousCommand );
	}

	ContextPrivate::CommandInfo & commandInfo = d_->commandInfos[ command ];
	Q_ASSERT( !commandInfo.isNull() );

	d_->commandForButton[ button ] = command;
	commandInfo.boundButtons << button;

	emit bindChanged( command );
}


void Context::attachAxisDirectionToCommand( const AxisDirection & axisDirection, const int command )
{
	if ( d_->commandForAxisDirection.value( axisDirection ) == command )
		return;

	{
		const int previousAxisCommand = d_->detachAxis( axisDirection.axis() );
		if ( previousAxisCommand != 0 )
			emit bindChanged( previousAxisCommand );

		const int previousCommand = d_->detachAxisDirection( axisDirection );
		if ( previousCommand != 0 )
			emit bindChanged( previousCommand );
	}

	ContextPrivate::CommandInfo & commandInfo = d_->commandInfos[ command ];
	Q_ASSERT( !commandInfo.isNull() );

	d_->commandForAxisDirection[ axisDirection ] = command;
	commandInfo.boundAxisDirections << axisDirection;

	emit bindChanged( command );
}


void Context::attachAxisToCommand( const Axis & axis, const int command )
{
	if ( d_->commandForAxis.value( axis ) == command )
		return;

	{
		const int previousCommand = d_->detachAxis( axis );
		if ( previousCommand != 0 )
			emit bindChanged( previousCommand );

		const int previousBackwardCommand = d_->detachAxisDirection( AxisDirection( axis, false ) );
		if ( previousBackwardCommand != 0 )
			emit bindChanged( previousBackwardCommand );

		const int previousForwardCommand = d_->detachAxisDirection( AxisDirection( axis, true ) );
		if ( previousForwardCommand != 0 )
			emit bindChanged( previousForwardCommand );
	}

	ContextPrivate::CommandInfo & commandInfo = d_->commandInfos[ command ];
	Q_ASSERT( !commandInfo.isNull() );

	d_->commandForAxis[ axis ] = command;
	commandInfo.boundAxes << axis;

	emit bindChanged( command );
}


void Context::detachButton( const Button & button )
{
	const int command = d_->detachButton( button );
	if ( command != 0 )
		emit bindChanged( command );
}


void Context::detachAxis( const Axis & axis )
{
	const int command = d_->detachAxis( axis );
	if ( command != 0 )
		emit bindChanged( command );
}


void Context::detachAxisDirection( const AxisDirection & axisDirection )
{
	const int command = d_->detachAxisDirection( axisDirection );
	if ( command != 0 )
		emit bindChanged( command );
}


void Context::detachCommand( const int command )
{
	ContextPrivate::CommandInfo & commandInfo = d_->commandInfos[ command ];
	Q_ASSERT( !commandInfo.isNull() );

	foreach ( const Button & button, commandInfo.boundButtons )
		d_->commandForButton.remove( button );
	foreach ( const AxisDirection & axisDirection, commandInfo.boundAxisDirections )
		d_->commandForAxisDirection.remove( axisDirection );
	foreach ( const Axis & axis, commandInfo.boundAxes )
		d_->commandForAxis.remove( axis );

	commandInfo.boundButtons.clear();
	commandInfo.boundAxisDirections.clear();
	commandInfo.boundAxes.clear();

	emit bindChanged( command );
}


void Context::detachAll()
{
	d_->commandForButton.clear();
	d_->commandForAxisDirection.clear();
	d_->commandForAxis.clear();

	for ( Tools::IdGeneratorIterator it( d_->manager->d_->commandIdGenerator ); it.hasNext(); )
	{
		ContextPrivate::CommandInfo & commandInfo = d_->commandInfos[ it.next() ];
		Q_ASSERT( !commandInfo.isNull() );
		commandInfo.boundButtons.clear();
		commandInfo.boundAxisDirections.clear();
		commandInfo.boundAxes.clear();
	}

	for ( Tools::IdGeneratorIterator it( d_->manager->d_->commandIdGenerator ); it.hasNext(); )
		emit bindChanged( it.next() );
}


QList<Button> Context::buttonsForCommand( const int command ) const
{
	const ContextPrivate::CommandInfo & commandInfo = d_->commandInfos.at( command );
	Q_ASSERT( !commandInfo.isNull() );
	return commandInfo.boundButtons;
}


QList<AxisDirection> Context::axisDirectionsForCommand( const int command ) const
{
	const ContextPrivate::CommandInfo & commandInfo = d_->commandInfos.at( command );
	Q_ASSERT( !commandInfo.isNull() );
	return commandInfo.boundAxisDirections;
}


QList<Axis> Context::axesForCommand( const int command ) const
{
	const ContextPrivate::CommandInfo & commandInfo = d_->commandInfos.at( command );
	Q_ASSERT( !commandInfo.isNull() );
	return commandInfo.boundAxes;
}




int ManagerPrivate::createCommand()
{
	const int command = commandIdGenerator.take();

	// resize self
	if ( commandInfos.size() <= command )
	{
		if ( commandInfos.capacity() <= command )
			commandInfos.reserve( command + 16 );
	}
	commandInfos.resize( command + 1 );

	CommandInfo & commandInfo = commandInfos[ command ];
	Q_ASSERT( commandInfo.isNull() );
	commandInfo.command = command;

	// resize contexts
	foreach ( Context * const context, contexts )
	{
		QVector<ContextPrivate::CommandInfo> & commandInfos = context->d_->commandInfos;
		if ( commandInfos.size() <= command )
		{
			if ( commandInfos.capacity() <= command )
				commandInfos.reserve( command + 16 );
		}
		commandInfos.resize( command + 1 );

		ContextPrivate::CommandInfo & commandInfo = commandInfos[ command ];
		Q_ASSERT( commandInfo.isNull() );
		commandInfo.command = command;
	}

	return command;
}




Manager::Manager( QObject * const parent ) :
	QObject( parent ),
	d_( new ManagerPrivate( this ) )
{
}


Manager::~Manager()
{
	QList<Context*> copy = d_->contexts;
	qDeleteAll( copy );

	qDeleteAll( d_->devices );

	delete d_;
}


int Manager::_registerDevice( Device * const device )
{
	device->d_->type_ = d_->typeIdGenerator.take();
	if ( d_->deviceForType.size() <= device->d_->type() )
		d_->deviceForType.resize( device->d_->type() + 1 );
	d_->deviceForType[ device->d_->type() ] = device;
	return device->d_->type();
}


int Manager::createDevice( const int type )
{
	Device * const device = d_->deviceForType.value( type );
	Q_ASSERT( device );

	ManagerPrivate::DeviceInfo info;
	info.deviceId = d_->deviceIdGenerator.take();
	info.device = device;

	if ( d_->deviceInfoForId.size() <= info.deviceId )
		d_->deviceInfoForId.resize( info.deviceId + 1 );

	d_->deviceInfoForId[ info.deviceId ] = info;

	return info.deviceId;
}


void Manager::removeDevice( const int deviceId )
{
	ManagerPrivate::DeviceInfo & info = d_->deviceInfoForId[ deviceId ];
	Q_ASSERT( !info.isNull() );

	d_->deviceIdGenerator.free( info.deviceId );
	info = ManagerPrivate::DeviceInfo();
}


Button Manager::createButton( const int deviceId, const int button ) const
{
	Q_ASSERT( !d_->deviceInfoForId.at( deviceId ).isNull() );
	return Button( deviceId, button, this );
}


Axis Manager::createAxis( const int deviceId, const int axis ) const
{
	Q_ASSERT( !d_->deviceInfoForId.at( deviceId ).isNull() );
	return Axis( deviceId, axis, this );
}


int Manager::createCommand()
{
	const int command = d_->createCommand();
	return command;
}


int Manager::createToggleCommand()
{
	const int command = d_->createCommand();
	d_->commandInfos[ command ].isToggle = true;
	return command;
}


int Manager::createAxisCommand()
{
	const int command = d_->createCommand();
	d_->commandInfos[ command ].isAxis = true;
	return command;
}


bool Manager::isCommandToggle( const int command ) const
{
	const ManagerPrivate::CommandInfo & commandInfo = d_->commandInfos.at( command );
	Q_ASSERT( !commandInfo.isNull() );
	return commandInfo.isToggle;
}


bool Manager::isCommandAxis( const int command ) const
{
	const ManagerPrivate::CommandInfo & commandInfo = d_->commandInfos.at( command );
	Q_ASSERT( !commandInfo.isNull() );
	return commandInfo.isAxis;
}


#if 0
int Manager::createCommandGroup()
{
	const int groupId = d_->commandGroupIdGenerator.take();

	if ( d_->commandGroupInfoForId.size() <= groupId )
		d_->commandGroupInfoForId.resize( groupId + 1 );

	ManagerPrivate::CommandGroupInfo groupInfo;
	groupInfo.enabled = false;
	d_->commandGroupInfoForId[ groupId ] = groupInfo;

	return groupId;
}


void Manager::addCommandToGroup( int groupId, const QString & command )
{
	Q_ASSERT( groupId > 0 && groupId < d_->commandGroupInfoForId.size() );
	ManagerPrivate::CommandGroupInfo & groupInfo = d_->commandGroupInfoForId[ groupId ];
	groupInfo.commands << command;
	d_->groupIdForCommand[ command ] = groupId;
}


void Manager::setCommandGroupEnabled( int groupId, bool enabled )
{
	Q_ASSERT( groupId > 0 && groupId < d_->commandGroupInfoForId.size() );
	d_->commandGroupInfoForId[ groupId ].enabled = enabled;
}
#endif


Context * Manager::createContext()
{
	Context * const context = new Context( this );

	// initialize command infos
	QVector<ContextPrivate::CommandInfo> commandInfos = context->d_->commandInfos;
	commandInfos.resize( d_->commandInfos.size() );

	for ( Tools::IdGeneratorIterator it( d_->commandIdGenerator ); it.hasNext(); )
	{
		const int command = it.next();
		ContextPrivate::CommandInfo & commandInfo = context->d_->commandInfos[ command ];
		Q_ASSERT( commandInfo.isNull() );
		commandInfo.command = command;
	}

	d_->contexts << context;
	return context;
}


QList<Context*> Manager::currentContexts() const
{
	return d_->currentContexts;
}


void Manager::setCurrentContexts( const QList<Context*> & contexts )
{
	QSet<Context*> set;
	foreach ( Context * const context, contexts )
	{
		Q_ASSERT( d_->contexts.contains( context ) );
		Q_ASSERT( !set.contains( context ) );
		set << context;
	}

	d_->currentContexts = contexts;
}


void ManagerPrivate::handleButton( CommandInfo & commandInfo, const bool isDown )
{
	Q_ASSERT( !commandInfo.isAxis );

	if ( commandInfo.isDown == Tools::cleanBoolean( isDown ) )
	{
		// down state not changed, ignore
		return;
	}

	commandInfo.isDown = Tools::cleanBoolean( isDown );

	if ( !commandInfo.isToggle && !isDown )
	{
		// button release does nothing for regular non-toggle command
		return;
	}

	if ( !commandInfo.isToggle )
	{
		// regular command, value is ignored
		emit manager->commandTriggered( commandInfo.command );
	}
	else
	{
		// toggle command, value is 1 on press and 0 on release
		emit manager->toggleCommandTriggered( commandInfo.command, isDown );
	}
}


void ManagerPrivate::handleAxis( CommandInfo & commandInfo, const qreal value )
{
	Q_ASSERT( commandInfo.isAxis );

	if ( commandInfo.value == value )
	{
		// value not changed, ignore
		return;
	}

	commandInfo.value = value;
	emit manager->axisCommandTriggered( commandInfo.command, value );
}


bool Manager::handleButton( const Button & button, const bool isPressed )
{
	foreach ( Context * const context, d_->currentContexts )
	{
		const int command = context->d_->commandForButton.value( button );
		if ( command == 0 )
			continue;

		ManagerPrivate::CommandInfo & commandInfo = d_->commandInfos[ command ];
		Q_ASSERT( !commandInfo.isNull() );

		if ( commandInfo.isAxis )
		{
			// TODO: Implement button directions
		}
		else
		{
			d_->handleButton( commandInfo, Tools::cleanBoolean( isPressed ) );
		}
		return true;
	}

	return false;
}


bool Manager::handleAxis( const Axis & axis, const qreal value )
{
	foreach ( Context * const context, d_->currentContexts )
	{
		// check the axis direction first
		const int backwardCommand = context->d_->commandForAxisDirection.value( AxisDirection( axis, false ) );
		if ( backwardCommand != 0 )
		{
			ManagerPrivate::CommandInfo & commandInfo = d_->commandInfos[ backwardCommand ];
			Q_ASSERT( !commandInfo.isNull() );

			if ( commandInfo.isAxis )
			{
				// TODO: Implement button directions
			}
			else
			{
				d_->handleButton( commandInfo, value < 0 );
			}
		}

		const int forwardCommand = context->d_->commandForAxisDirection.value( AxisDirection( axis, true ) );
		if ( forwardCommand != 0 )
		{
			ManagerPrivate::CommandInfo & commandInfo = d_->commandInfos[ forwardCommand ];
			Q_ASSERT( !commandInfo.isNull() );

			if ( commandInfo.isAxis )
			{
				// TODO: Implement button directions
			}
			else
			{
				d_->handleButton( commandInfo, value > 0 );
			}
		}

		if ( backwardCommand != 0 || forwardCommand != 0 )
			return true;

		// check the regular axis
		const int command = context->d_->commandForAxis.value( axis );
		if ( command != 0 )
		{
			ManagerPrivate::CommandInfo & commandInfo = d_->commandInfos[ command ];
			Q_ASSERT( !commandInfo.isNull() );

			if ( !commandInfo.isAxis )
			{
				d_->handleButton( commandInfo, value != 0 );
			}
			else
			{
				d_->handleAxis( commandInfo, value );
			}
			return true;
		}
	}

	return false;
}




}} // Grim::Bind
