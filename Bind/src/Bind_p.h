
#pragma once

#include "Bind.h"

#include <QVector>
#include <QHash>
#include <QStringList>

#include <Grim/Tools/IdGenerator.h>




namespace Grim { namespace Bind {




class DevicePrivate
{
public:
	class ButtonInfo
	{
	public:
		ButtonInfo() :
			button( 0 ), translationName( 0 )
		{}

		bool isNull() const
		{ return button == 0; }

		int button;
		QString string;
		const char * translationName;
	};

	class AxisInfo
	{
	public:
		AxisInfo() :
			axis( 0 ), translationName( 0 )
		{}

		bool isNull() const
		{ return axis == 0; }

		int axis;
		QString string;
		const char * translationName;
	};

	DevicePrivate();
	virtual ~DevicePrivate() {}

	int type() const;

private:
	int type_;

	friend class Manager;
};




class ContextPrivate
{
public:
	class CommandInfo
	{
	public:
		CommandInfo() :
			command( 0 )
		{}

		bool isNull() const
		{ return command == 0; }

		// parameters
		int command;

		// bind
		QList<Button> boundButtons;
		QList<Axis> boundAxes;
		QList<AxisDirection> boundAxisDirections;
	};

	ContextPrivate( Context * const _context, Manager * const _manager ) :
		context( _context ), manager( _manager )
	{}

	int detachButton( const Button & button );
	int detachAxis( const Axis & axis );
	int detachAxisDirection( const AxisDirection & axisDirection );

public:
	Context * const context;
	Manager * const manager;

	// commands
	QVector<CommandInfo> commandInfos;

	// helpers
	QHash<Button, int> commandForButton;
	QHash<Axis, int> commandForAxis;
	QHash<AxisDirection, int> commandForAxisDirection;
};




class ManagerPrivate
{
public:
	class DeviceInfo
	{
	public:
		DeviceInfo() :
			deviceId( 0 ), device( 0 )
		{}

		bool isNull() const
		{ return deviceId == 0; }

		int deviceId;
		Device * device;
	};

	class CommandInfo
	{
	public:
		CommandInfo() :
			command( 0 ), isToggle( false ), isAxis( false ),
			isDown( false ), value( 0 )
		{}

		bool isNull() const
		{ return command == 0; }

		// parameters
		int command;
		bool isToggle;
		bool isAxis;

		// state
		bool isDown;
		qreal value;
	};

	ManagerPrivate( Manager * _manager ) :
		manager( _manager )
	{}

	void handleButton( CommandInfo & commandInfo, bool isDown );
	void handleAxis( CommandInfo & commandInfo, qreal value );

	int createCommand();

	Manager * const manager;

	Tools::IdGenerator typeIdGenerator;
	QList<Device*> devices;
	QVector<Device*> deviceForType;

	Tools::IdGenerator deviceIdGenerator;
	QVector<DeviceInfo> deviceInfoForId;

#if 0
	class CommandGroupInfo
	{
	public:
		CommandGroupInfo() :
			enabled( false )
		{}

		QStringList commands;
		bool enabled;
	};

	Tools::IdGenerator commandGroupIdGenerator;
	QVector<CommandGroupInfo> commandGroupInfoForId;
	QHash<QString,int> groupIdForCommand;
#endif

	QList<Context*> contexts;
	QList<Context*> currentContexts;

	// commands
	Tools::IdGenerator commandIdGenerator;
	QVector<CommandInfo> commandInfos;
};




inline DevicePrivate::DevicePrivate() :
	type_( 0 )
{}

inline int DevicePrivate::type() const
{ return type_; }




}} // Grim::Bind
