
#include "Gamepad.h"
#include "Gamepad_p.h"

#include <windows.h>
#include <regstr.h>




namespace Grim {
namespace Gamepad {




static QString joycapsDeviceName( const JOYCAPS & joycaps )
{
	return QString::fromUtf16( (const ushort*)joycaps.szPname );
}


static QString registryDeviceName( const JoystickId joystickId, const JOYCAPS & joycaps )
{
	const QString keyRegPath = QString().sprintf( "%ls\\%ls\\%ls", REGSTR_PATH_JOYCONFIG, joycaps.szRegKey, REGSTR_KEY_JOYCURR );

	HKEY rootKey = HKEY_LOCAL_MACHINE;
	HKEY key;
	LONG result = ::RegOpenKeyExW( rootKey, (const wchar_t*)keyRegPath.utf16(), 0, KEY_READ, &key );
	if ( result != ERROR_SUCCESS )
	{
		rootKey = HKEY_CURRENT_USER;
		result = ::RegOpenKeyExW( rootKey, (const wchar_t*)keyRegPath.utf16(), 0, KEY_READ, &key );
		if ( result != ERROR_SUCCESS )
			return QString();
	}

	const QString valueRegPath = QString().sprintf( "Joystick%d%ls", int(joystickId + 1), REGSTR_VAL_JOYOEMNAME );

	wchar_t regName[256];
	DWORD regNameSize = sizeof(regName);
	result = ::RegQueryValueExW( key, (const wchar_t*)valueRegPath.utf16(), 0, 0, (LPBYTE)regName, &regNameSize );
	::RegCloseKey( key );

	if ( result != ERROR_SUCCESS )
		return QString();

	const QString valueSubKeyPath = QString().sprintf( "%ls\\%ls", REGSTR_PATH_JOYOEM, regName );
	result = ::RegOpenKeyExW( rootKey, (const wchar_t*)valueSubKeyPath.utf16(), 0, KEY_READ, &key );

	if ( result != ERROR_SUCCESS )
		return QString();

	QString name;
	regNameSize = sizeof(regName);
	result = ::RegQueryValueExW( key, REGSTR_VAL_JOYOEMNAME, 0, 0, 0, &regNameSize );
	if ( result == ERROR_SUCCESS )
	{
		DWORD regNameSize = sizeof(regName);
		result = ::RegQueryValueExW( key, REGSTR_VAL_JOYOEMNAME, 0, 0, (LPBYTE)regName, &regNameSize );
		name = QString::fromUtf16( (const ushort*)regName );
	}
	::RegCloseKey( key );

	return name;
}


void ManagerPrivate::updateDevices()
{
	const int deviceCount = joyGetNumDevs();
	for ( qint64 deviceIndex = 0; deviceIndex < deviceCount; ++deviceIndex )
	{
		const JoystickId joystickId = JOYSTICKID1 + deviceIndex;

		JOYINFOEX joyinfo;
		joyinfo.dwSize = sizeof(joyinfo);
		joyinfo.dwFlags = JOY_RETURNALL;
		MMRESULT result = joyGetPosEx( joystickId, &joyinfo );
		if ( result == JOYERR_UNPLUGGED )
		{
			// check the device was in list and remove if so
			Device * const device = deviceForJoystickId_.value( joystickId );
			if ( device )
				_deviceDetached( device );
			continue;
		}

		if ( result != JOYERR_NOERROR )
			continue;

		JOYCAPS joycaps;
		if ( joyGetDevCaps( joystickId, &joycaps, sizeof(joycaps) ) != JOYERR_NOERROR )
			continue;

		// check that this device already present
		if ( deviceForJoystickId_.contains( joystickId ) )
			continue;

		Device * const device = new Device( deviceIdGenerator_.take(), manager_ );
		DevicePrivate * const devicePrivate = device->d_;

		devicePrivate->joystickId_ = joystickId;

		// fill descriptable info
		devicePrivate->vendorId_ = joycaps.wMid;
		devicePrivate->productId_ = joycaps.wPid;
		devicePrivate->name_ = registryDeviceName( joystickId, joycaps );
		if ( devicePrivate->name_.isNull() )
			devicePrivate->name_ = joycapsDeviceName( joycaps );

		// axes
		devicePrivate->axisInfos_[ Axis_X ].isValid = true;
		devicePrivate->axisInfos_[ Axis_X ].minValue = joycaps.wXmin;
		devicePrivate->axisInfos_[ Axis_X ].maxValue = joycaps.wXmax;

		devicePrivate->axisInfos_[ Axis_Y ].isValid = true;
		devicePrivate->axisInfos_[ Axis_Y ].minValue = joycaps.wYmin;
		devicePrivate->axisInfos_[ Axis_Y ].maxValue = joycaps.wYmax;

		if ( joycaps.wCaps & JOYCAPS_HASZ )
		{
			devicePrivate->axisInfos_[ Axis_Z ].isValid = true;
			devicePrivate->axisInfos_[ Axis_Z ].minValue = joycaps.wZmin;
			devicePrivate->axisInfos_[ Axis_Z ].maxValue = joycaps.wZmax;
		}

		if ( joycaps.wCaps & JOYCAPS_HASR )
		{
			devicePrivate->axisInfos_[ Axis_R ].isValid = true;
			devicePrivate->axisInfos_[ Axis_R ].minValue = joycaps.wRmin;
			devicePrivate->axisInfos_[ Axis_R ].maxValue = joycaps.wRmax;
		}

		if ( joycaps.wCaps & JOYCAPS_HASU )
		{
			devicePrivate->axisInfos_[ Axis_U ].isValid = true;
			devicePrivate->axisInfos_[ Axis_U ].minValue = joycaps.wUmin;
			devicePrivate->axisInfos_[ Axis_U ].maxValue = joycaps.wUmax;
		}

		if ( joycaps.wCaps & JOYCAPS_HASV )
		{
			devicePrivate->axisInfos_[ Axis_V ].isValid = true;
			devicePrivate->axisInfos_[ Axis_V ].minValue = joycaps.wVmin;
			devicePrivate->axisInfos_[ Axis_V ].maxValue = joycaps.wVmax;
		}

		if ( joycaps.wCaps & JOYCAPS_HASPOV )
		{
			devicePrivate->axisInfos_[ Axis_PovX ].isValid = true;
			devicePrivate->axisInfos_[ Axis_PovX ].minValue = -1;
			devicePrivate->axisInfos_[ Axis_PovX ].maxValue = 1;

			devicePrivate->axisInfos_[ Axis_PovY ].isValid = true;
			devicePrivate->axisInfos_[ Axis_PovY ].minValue = -1;
			devicePrivate->axisInfos_[ Axis_PovY ].maxValue = 1;
		}

		for ( int axisType = 0; axisType < Axis_TotalAxes; ++axisType )
		{
			AxisInfo & axisInfo = devicePrivate->axisInfos_[ axisType ];
			if ( !axisInfo.isValid )
				continue;
			axisInfo.range = axisInfo.maxValue - axisInfo.minValue;
			if ( axisInfo.range <= 0 )
			{
				axisInfo.isValid = false;
				continue;
			}
		}

		// buttons
#if 0
		devicePrivate->buttonInfos_.resize( qMin<int>( joycaps.wNumButtons, sizeof(DWORD)*8 ) );
		for ( int button = 0; button < devicePrivate->buttonInfos_.size(); ++button )
			devicePrivate->buttonInfos_[ button ].on = joyinfo.dwButtons & (1 << button);
#else
//#	error "Implement button mapping under Windows"
#endif

		// save joyinfo
		devicePrivate->joyinfo_ = joyinfo;

		// save device
		_deviceAttached( device );
	}
}


static void povToValues( const DWORD pov, int & x, int & y )
{
	if ( pov == JOY_POVCENTERED )
	{
		x = 0;
		y = 0;
		return;
	}

	if ( pov > JOY_POVFORWARD && pov < JOY_POVBACKWARD )
		x = 1;
	else if ( pov > JOY_POVBACKWARD )
		x = -1;
	else
		x = 0;

	if ( pov > JOY_POVLEFT || pov < JOY_POVRIGHT )
		y = 1;
	else if ( pov > JOY_POVRIGHT && pov < JOY_POVLEFT )
		y = -1;
	else
		y = 0;
}


void ManagerPrivate::_povChangedWin( Device * const device, const DWORD pov )
{
	AxisInfo & povXAxis = device->d_->axisInfos_[ Axis_PovX ];
	AxisInfo & povYAxis = device->d_->axisInfos_[ Axis_PovY ];
	if ( !povXAxis.isValid || !povYAxis.isValid )
		return;

	int x, y;
	povToValues( pov, x, y );

	_axisChanged( device, Axis_PovX, x );
	_axisChanged( device, Axis_PovY, y );
}


void ManagerPrivate::_buttonsChangedWin( Device * const device, const DWORD buttons )
{
#if 0
	const int buttonCount = qMin<int>( device->d_->buttonInfos_.size(), sizeof(buttons)*8 );
	const DWORD changedButtons = device->d_->joyinfo_.dwButtons ^ buttons;
	for ( int button = 0; button < buttonCount; ++button )
	{
		const DWORD buttonFlag = 1 << button;
		if ( !(changedButtons & buttonFlag) )
			continue;
		_buttonChanged( device, button, (buttons & buttonFlag) ? true : false );
	}
#else
//#	error "Implement button mapping under Windows"
#endif
}


void ManagerPrivate::_processActiveDevicesWin()
{
	QList<int> devicesToRemove;
	for ( Tools::IdGeneratorIterator deviceIt( deviceIdGenerator_ ); deviceIt.hasNext(); )
	{
		const int deviceId = deviceIt.next();

		Device * const device = deviceForId_.at( deviceId );
		DevicePrivate * const devicePrivate = device->d_;

		JOYINFOEX joyinfo;
		memset( &joyinfo, 0, sizeof(joyinfo) );
		joyinfo.dwSize = sizeof(joyinfo);
		joyinfo.dwFlags = JOY_RETURNALL;
		MMRESULT result = joyGetPosEx( devicePrivate->joystickId_, &joyinfo );

		if ( result == JOYERR_UNPLUGGED )
		{
			devicesToRemove << deviceId;
			continue;
		}

		if ( result != JOYERR_NOERROR )
			continue;

		if ( !devicePrivate->isActive_ )
			continue;

		if ( joyinfo.dwXpos != devicePrivate->joyinfo_.dwXpos )
			_axisChanged( device, Axis_X, joyinfo.dwXpos );

		if ( joyinfo.dwYpos != devicePrivate->joyinfo_.dwYpos )
			_axisChanged( device, Axis_Y, joyinfo.dwYpos );

		if ( joyinfo.dwZpos != devicePrivate->joyinfo_.dwZpos )
			_axisChanged( device, Axis_Z, joyinfo.dwZpos );

		if ( joyinfo.dwRpos != devicePrivate->joyinfo_.dwRpos )
			_axisChanged( device, Axis_RX, joyinfo.dwRpos );

		if ( joyinfo.dwUpos != devicePrivate->joyinfo_.dwUpos )
			_axisChanged( device, Axis_RY, joyinfo.dwUpos );

		if ( joyinfo.dwVpos != devicePrivate->joyinfo_.dwVpos )
			_axisChanged( device, Axis_RZ, joyinfo.dwVpos );

		if ( joyinfo.dwPOV != devicePrivate->joyinfo_.dwPOV )
			_povChangedWin( device, joyinfo.dwPOV );

		if ( joyinfo.dwButtons != devicePrivate->joyinfo_.dwButtons )
			_buttonsChangedWin( device, joyinfo.dwButtons );

		devicePrivate->joyinfo_ = joyinfo;
	}

	// remove detached devices
	foreach ( int deviceId, devicesToRemove )
		_deviceDetached( deviceForId_.at( deviceId ) );
}




} // namespace Gamepad
} // namespace Grim
