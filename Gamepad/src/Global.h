
#pragma once

#include <qglobal.h>




#ifdef Q_OS_WIN
#	ifdef GRIM_GAMEPAD_STATIC
#		define GRIM_GAMEPAD_EXPORT
#	else
#		ifdef GRIM_GAMEPAD_BUILD
#			define GRIM_GAMEPAD_EXPORT Q_DECL_EXPORT
#		else
#			define GRIM_GAMEPAD_EXPORT Q_DECL_IMPORT
#		endif
#	endif
#else
#	define GRIM_GAMEPAD_EXPORT __attribute__ ((visibility("default")))
#endif
