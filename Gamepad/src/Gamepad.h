
#pragma once

#include "Global.h"

#include <QObject>
#include <QEvent>

#include <Grim/Debug/Log.h>




namespace Grim {
namespace Gamepad {




class ButtonEventPrivate;
class AxisEventPrivate;
class DevicePrivate;
class ManagerPrivate;
class Manager;




enum ButtonType
{
	Button_Null = 0,

	Button_A,
	Button_B,
	Button_C,
	Button_X,
	Button_Y,
	Button_Z,
	Button_TL,
	Button_TR,
	Button_TL2,
	Button_TR2,
	Button_Select,
	Button_Start,
	Button_Mode,
	Button_ThumbL,
	Button_ThumbR,

	Button_TotalButtons
};




enum AxisType
{
	Axis_Null = 0,

	Axis_X,
	Axis_Y,
	Axis_Z,
	Axis_R,
	Axis_U,
	Axis_V,
	Axis_RX,
	Axis_RY,
	Axis_RZ,
	Axis_PovX,
	Axis_PovY,

	Axis_TotalAxes
};




enum EventType
{
	Event_Button = QEvent::User + 1,
	Event_Axis
};




class GRIM_GAMEPAD_EXPORT ButtonEvent : public QEvent
{
public:
	ButtonType buttonType() const;
	bool isDown() const;

private:
	Q_DISABLE_COPY(ButtonEvent)

	ButtonEvent( ButtonEventPrivate * d );
	ButtonEventPrivate * const d_;

	friend class ManagerPrivate;
};




class GRIM_GAMEPAD_EXPORT AxisEvent : public QEvent
{
public:
	AxisType axisType() const;
	qreal value() const;
	qreal oldValue() const;

private:
	Q_DISABLE_COPY(AxisEvent)

	AxisEvent( AxisEventPrivate * d );
	AxisEventPrivate * const d_;

	friend class ManagerPrivate;
};




class GRIM_GAMEPAD_EXPORT Device : public QObject
{
	Q_OBJECT

public:
	~Device();

	int deviceId() const;

	QString name() const;
	int vendorId() const;
	int productId() const;

	void setAxisMapping( AxisType axisType, qreal min, qreal max );

	qreal axisDeadZone( AxisType axisType ) const;
	void setAxisDeadZone( AxisType axisType, qreal value );

	bool isActive() const;
	void setActive( bool set );

private:
	Device( int id, Manager * manager );

private:
	DevicePrivate * const d_;

	friend class ManagerPrivate;
	friend class DeviceThread;
};




class GRIM_GAMEPAD_EXPORT Manager : public QObject
{
	Q_OBJECT

public:
	Manager( QObject * parent = 0 );
	~Manager();

	QList<int> deviceIds() const;
	Device * deviceForId( int deviceId ) const;

public slots:
	void updateDevices();

signals:
	void deviceAttached( int deviceId );
	void deviceDetached( int deviceId );

private:
	ManagerPrivate * const d_;

	friend class ManagerPrivate;
	friend class Device;
	friend class DevicePrivate;
};




} // namespace Gamepad
} // namespace Grim




namespace Grim {
namespace Debug {

template <>
inline void Log::append<Grim::Gamepad::ButtonType>( const Grim::Gamepad::ButtonType & buttonType )
{
	State state( *this );
	Q_UNUSED( state );

	maybeSpace();
	*this << NoSpace();

#define GRIM_GAMEPAD_LOG_BUTTON_TYPE(x) \
	case Grim::Gamepad::x: appendString( #x ); return

	switch ( buttonType )
	{
	GRIM_GAMEPAD_LOG_BUTTON_TYPE(Button_Null);
	GRIM_GAMEPAD_LOG_BUTTON_TYPE(Button_A);
	GRIM_GAMEPAD_LOG_BUTTON_TYPE(Button_B);
	GRIM_GAMEPAD_LOG_BUTTON_TYPE(Button_C);
	GRIM_GAMEPAD_LOG_BUTTON_TYPE(Button_X);
	GRIM_GAMEPAD_LOG_BUTTON_TYPE(Button_Y);
	GRIM_GAMEPAD_LOG_BUTTON_TYPE(Button_Z);
	GRIM_GAMEPAD_LOG_BUTTON_TYPE(Button_TL);
	GRIM_GAMEPAD_LOG_BUTTON_TYPE(Button_TR);
	GRIM_GAMEPAD_LOG_BUTTON_TYPE(Button_TL2);
	GRIM_GAMEPAD_LOG_BUTTON_TYPE(Button_TR2);
	GRIM_GAMEPAD_LOG_BUTTON_TYPE(Button_Select);
	GRIM_GAMEPAD_LOG_BUTTON_TYPE(Button_Start);
	GRIM_GAMEPAD_LOG_BUTTON_TYPE(Button_Mode);
	GRIM_GAMEPAD_LOG_BUTTON_TYPE(Button_ThumbL);
	GRIM_GAMEPAD_LOG_BUTTON_TYPE(Button_ThumbR);

	case Grim::Gamepad::Button_TotalButtons:
		break;
	}

#undef GRIM_GAMEPAD_LOG_BUTTON_TYPE

	*this << "(Invalid button type: " << int(buttonType) << ")";
}


template <>
inline void Log::append<Grim::Gamepad::AxisType>( const Grim::Gamepad::AxisType & axisType )
{
	State state( *this );
	Q_UNUSED( state );

	maybeSpace();
	*this << NoSpace();

#define GRIM_GAMEPAD_LOG_AXIS_TYPE(x) \
	case Grim::Gamepad::x: appendString( #x ); return

	switch ( axisType )
	{
	GRIM_GAMEPAD_LOG_AXIS_TYPE(Axis_Null);
	GRIM_GAMEPAD_LOG_AXIS_TYPE(Axis_X);
	GRIM_GAMEPAD_LOG_AXIS_TYPE(Axis_Y);
	GRIM_GAMEPAD_LOG_AXIS_TYPE(Axis_Z);
	GRIM_GAMEPAD_LOG_AXIS_TYPE(Axis_RX);
	GRIM_GAMEPAD_LOG_AXIS_TYPE(Axis_RY);
	GRIM_GAMEPAD_LOG_AXIS_TYPE(Axis_RZ);
	GRIM_GAMEPAD_LOG_AXIS_TYPE(Axis_PovX);
	GRIM_GAMEPAD_LOG_AXIS_TYPE(Axis_PovY);

	case Grim::Gamepad::Axis_TotalAxes:
		break;
	}

#undef GRIM_GAMEPAD_LOG_AXIS_TYPE

	*this << "(Invalid axis type: " << int(axisType) << ")";
}

} // namespace Debug
} // namespace Grim
