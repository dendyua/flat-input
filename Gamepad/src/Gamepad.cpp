
#include "Gamepad.h"
#include "Gamepad_p.h"

#ifdef Q_OS_LINUX
#include <unistd.h>
#endif

#include <QCoreApplication>

#include <Grim/Tools/Utils.h>




namespace Grim { namespace Gamepad {




ButtonEvent::ButtonEvent( ButtonEventPrivate * const d ) :
	QEvent( static_cast<QEvent::Type>( Event_Button ) ),
	d_( d )
{
}


ButtonType ButtonEvent::buttonType() const
{
	return d_->buttonType;
}


bool ButtonEvent::isDown() const
{
	return d_->isDown;
}




AxisEvent::AxisEvent( AxisEventPrivate * const d ) :
	QEvent( static_cast<QEvent::Type>( Event_Axis ) ),
	d_( d )
{
}


AxisType AxisEvent::axisType() const
{
	return d_->axisType;
}


qreal AxisEvent::value() const
{
	return d_->value;
}


qreal AxisEvent::oldValue() const
{
	return d_->oldValue;
}




DevicePrivate::DevicePrivate( Device * const device, const int id, Manager * const manager ) :
	device_( device ),
	id_( id ),
	manager_( manager )
{
	vendorId_ = 0;
	productId_ = 0;

	isActive_ = false;

#ifdef Q_OS_LINUX
	fileHandle_ = -1;
	thread_.device = device_;
	isAborted_ = false;
	isFileHandleBroken_ = false;
	fileWatcher_ = 0;
#endif

#ifdef Q_OS_WIN
	joystickId_ = -1;
#endif
}




Device::Device( const int id, Manager * const manager ) :
	d_( new DevicePrivate( this, id, manager ) )
{
	d_->buttonInfos_.resize( Button_TotalButtons );
	d_->axisInfos_.resize( Axis_TotalAxes );
}


Device::~Device()
{
#ifdef Q_OS_LINUX
	d_->_abortThread();

	if ( d_->fileWatcher_ )
		d_->_removeFileWatcher();

	if ( d_->fileHandle_ != 0 )
	{
		::close( d_->fileHandle_ );
		d_->fileHandle_ = 0;
	}
#endif

	delete d_;
}


int Device::deviceId() const
{
	return d_->id_;
}


QString Device::name() const
{
	return d_->name_;
}


int Device::vendorId() const
{
	return d_->vendorId_;
}


int Device::productId() const
{
	return d_->productId_;
}


void Device::setAxisMapping( const AxisType axisType, const qreal min, const qreal max )
{
	Q_ASSERT( axisType > 0 && axisType < Axis_TotalAxes );
	AxisInfo & axisInfo = d_->axisInfos_[ axisType ];
	axisInfo.minMapping = min;
	axisInfo.maxMapping = max;
	axisInfo.mappingLeft = qMin( axisInfo.minMapping, axisInfo.maxMapping );
	axisInfo.mappingRight = qMax( axisInfo.minMapping, axisInfo.maxMapping );
	axisInfo.mappingRange = axisInfo.maxMapping - axisInfo.minMapping;
}


qreal Device::axisDeadZone( const AxisType axisType ) const
{
	Q_ASSERT( axisType > 0 && axisType < Axis_TotalAxes );
	return d_->axisInfos_.at( axisType ).deadZone;
}


void Device::setAxisDeadZone( const AxisType axisType, const qreal value )
{
	Q_ASSERT( axisType > 0 && axisType < Axis_TotalAxes );
	d_->axisInfos_[ axisType ].deadZone = qBound<qreal>( 0.0, value, 1.0 );
}


bool Device::isActive() const
{
	return d_->isActive_;
}


void Device::setActive( const bool set )
{
	if ( Tools::cleanBoolean( set ) == d_->isActive_ )
		return;

	d_->isActive_ = Tools::cleanBoolean( set );

	d_->manager_->d_->deviceActivated( d_->isActive_ );

	if ( d_->isActive_ )
	{
#ifdef Q_OS_LINUX
		d_->_removeFileWatcher();

		d_->isAborted_ = false;
		d_->thread_.start();
#endif
	}
	else
	{
#ifdef Q_OS_LINUX
		d_->_abortThread();

		if ( !d_->isFileHandleBroken_ )
			d_->_createFileWatcher();
#endif

		QCoreApplication::removePostedEvents( d_, DevicePrivateEvent_Update );
		QCoreApplication::removePostedEvents( this, Event_Button );
		QCoreApplication::removePostedEvents( this, Event_Axis );
	}
}




ManagerPrivate::ManagerPrivate( Manager * const manager ) :
	manager_( manager )
{
	activeDeviceCount_ = 0;
}


ManagerPrivate::~ManagerPrivate()
{
	for ( Tools::IdGeneratorIterator it( deviceIdGenerator_ ); it.hasNext(); )
		delete deviceForId_.at( it.next() );

	deviceForId_.clear();
	deviceIdGenerator_ = Tools::IdGenerator();
	deviceIds_.clear();

#ifdef Q_OS_LINUX
	deviceForFilePath_.clear();
#endif

#ifdef Q_OS_WIN
	deviceForJoystickId_.clear();
#endif
}


void ManagerPrivate::deviceActivated( const bool on )
{
	if ( !on )
	{
		Q_ASSERT( activeDeviceCount_ > 0 );
		activeDeviceCount_--;

#ifdef Q_OS_WIN
		if ( activeDeviceCount_ == 0 )
			activeDevicesProcessTimer_.stop();
#endif
	}
	else
	{
		activeDeviceCount_++;

#ifdef Q_OS_WIN
		if ( activeDeviceCount_ == 1 )
			activeDevicesProcessTimer_.start( 1, this );
#endif
	}
}


void ManagerPrivate::timerEvent( QTimerEvent * const e )
{
#ifdef Q_OS_WIN
	if ( e->timerId() == activeDevicesProcessTimer_.timerId() )
	{
		_processActiveDevicesWin();
		return;
	}
#endif

	QObject::timerEvent( e );
}


void ManagerPrivate::_deviceAttached( Device * const device )
{
	if ( deviceForId_.size() <= device->deviceId() )
		deviceForId_.resize( device->deviceId() + 1 );
	deviceForId_[ device->deviceId() ] = device;

#ifdef Q_OS_LINUX
	deviceForFilePath_[ device->d_->filePath_ ] = device;
#endif

#ifdef Q_OS_WIN
	deviceForJoystickId_[ device->d_->joystickId_ ] = device;
#endif

	deviceIds_ << device->deviceId();

	// notify
	emit manager_->deviceAttached( device->deviceId() );
}


void ManagerPrivate::_deviceDetached( Device * const device )
{
	const int deviceId = device->deviceId();

	// notify
	emit manager_->deviceDetached( deviceId );

	device->setActive( false );

#ifdef Q_OS_LINUX
	deviceForFilePath_.remove( device->d_->filePath_ );
#endif

#ifdef Q_OS_WIN
	deviceForJoystickId_.remove( device->d_->joystickId_ );
#endif
	deviceIds_.removeOne( deviceId );
	deviceForId_[ deviceId ] = 0;

	deviceIdGenerator_.free( deviceId );

	device->deleteLater();
}


void ManagerPrivate::_buttonChanged( Device * const device, const ButtonType buttonType, const bool isDown )
{
	ButtonInfo & buttonInfo = device->d_->buttonInfos_[ buttonType ];
	if ( !buttonInfo.isValid )
		return;

	if ( buttonInfo.down == Tools::cleanBoolean( isDown ) )
		return;

	buttonInfo.down = Tools::cleanBoolean( isDown );

	ButtonEventPrivate buttonEventPrivate( buttonType, buttonInfo.down );
	ButtonEvent buttonEvent( &buttonEventPrivate );
	QCoreApplication::sendEvent( device, &buttonEvent );
}


void ManagerPrivate::_axisChanged( Device * const device, const AxisType axisType, const int value )
{
	AxisInfo & axisInfo = device->d_->axisInfos_[ axisType ];
	if ( !axisInfo.isValid )
		return;

	if ( axisInfo.value == value )
		return;

	const qreal oldValue = axisInfo.realValue;
	axisInfo.value = value;

	// Map value onto specified min/max range using linear transformation.
	// Standard canonic line formula used:
	//   (maxValue - minValue)/(value - minValue) = (maxMapping - minMapping)/(realValue - minMapping)
	//   thus
	//   realValue = (maxMapping - minMapping)*(value - minValue)/(maxValue - minValue) + minMapping
	//axisInfo.realValue = qBound( -1.0, qreal(value - axisInfo.minValue)/axisInfo.range*2.0 - 1.0, 1.0 );
	axisInfo.realValue = axisInfo.mappingRange*(axisInfo.value - axisInfo.minValue)/axisInfo.range + axisInfo.minMapping;

	// bound mapped value to allowed range
	axisInfo.realValue = qBound( axisInfo.mappingLeft, axisInfo.realValue, axisInfo.mappingRight );

	// check the dead zone
	if ( axisInfo.deadZone != 0 )
	{
		if ( axisInfo.realValue > -axisInfo.deadZone && axisInfo.realValue < axisInfo.deadZone )
			axisInfo.realValue = 0;
	}

	if ( oldValue == axisInfo.realValue )
		return;

	AxisEventPrivate axisEventPrivate( axisType, axisInfo.realValue, oldValue );
	AxisEvent axisEvent( &axisEventPrivate );
	QCoreApplication::sendEvent( device, &axisEvent );
}




Manager::Manager( QObject * const parent ) :
	QObject( parent ),
	d_( new ManagerPrivate( this ) )
{
}


Manager::~Manager()
{
	delete d_;
}


QList<int> Manager::deviceIds() const
{
	return d_->deviceIds_;
}


Device * Manager::deviceForId( const int deviceId ) const
{
	return d_->deviceForId_.at( deviceId );
}


void Manager::updateDevices()
{
	d_->updateDevices();
}




}} // Grim::Gamepad
